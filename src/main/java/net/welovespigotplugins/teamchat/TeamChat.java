package net.welovespigotplugins.teamchat;

import com.google.common.collect.Maps;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Plugin;
import net.welovespigotplugins.teamchat.command.TeamChatCommand;
import net.welovespigotplugins.teamchat.objects.PlayerInfo;

import java.util.Map;
import java.util.UUID;

/**
 * JavaDoc this file!
 * Created: 30.07.2018
 *
 * @author WeLoveSpigotPlugins (welovespigotplugins@gmail.com)
 */
public class TeamChat extends Plugin {

    public static final String PREFIX = "§8[§aTeamChat§8] ";
    private static TeamChat instance;
    private final Map<UUID, PlayerInfo> playerInfoMap = Maps.newConcurrentMap();

    @Override
    public void onEnable() {
        init();
    }

    private void init() {
        instance = this;
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new TeamChatCommand("tc"));
    }

    public static TeamChat getInstance() {
        return instance;
    }

    public PlayerInfo getPlayerInfo(final UUID uuid) {
        if (playerInfoMap.containsKey(uuid))
            return playerInfoMap.get(uuid);
        playerInfoMap.put(uuid, new PlayerInfo(ProxyServer.getInstance().getPlayer(uuid)));
        return playerInfoMap.get(uuid);
    }

}
