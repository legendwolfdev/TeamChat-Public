package net.welovespigotplugins.teamchat.objects;

import net.md_5.bungee.api.connection.ProxiedPlayer;

/**
 * JavaDoc this file!
 * Created: 30.07.2018
 *
 * @author WeLoveSpigotPlugins (welovespigotplugins@gmail.com)
 */
public class PlayerInfo {

    private final ProxiedPlayer proxiedPlayer;
    private boolean loggedIn = true;


    public PlayerInfo(final ProxiedPlayer proxiedPlayer) {
        this.proxiedPlayer = proxiedPlayer;
    }

    public boolean isLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(boolean loggedIn) {
        this.loggedIn = loggedIn;
    }
}
